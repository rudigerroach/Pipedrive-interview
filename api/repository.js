const mysql = require('promise-mysql');

module.exports = class DB {

    constructor(dbHost, dbName, dbUsername, dbPassword, dbConnection = null) {
        this.dbHost = dbHost;
        this.dbName = dbName;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
        this.connectionPool = dbConnection;
    }

    async getConnection() {
        if (!this.connectionPool) {
            this.connectionPool = await this.createPool();
        }
        return await this.connectionPool.getConnection();
    }

    async createPool() {
        return mysql.createPool({
            host: this.dbHost,
            user: this.dbUsername,
            password: this.dbPassword,
            database: this.dbName,
            connectionLimit: 10
        });
    }

    /**
     * returns the database ID of the persisted organization
     * @param organizationName
     * @returns {Promise<*>}
     */
    async addOrganization(organizationName) {
        let connection = await this.getConnection();
        let returnId;
        try {
            let insertionResult = await connection.query("INSERT INTO organization (name) VALUES (?)", [organizationName]);
            returnId = insertionResult.insertId;
        } catch (e) {
            let existingOrganization = await this.getOrganizationByName(organizationName);
            returnId = existingOrganization.id;
        }
        connection.release();
        return returnId;
    }

    async getOrganizationByName(organizationName) {
        let connection = await this.getConnection();
        let organization = await connection.query("select * from organization where name = ?", [organizationName]);
        connection.release();
        return organization[0];
    }

    async initializeOrganizationRelationshipNodes(parentName, daughterName) {
        let parentOrganizationId = await this.addOrganization(parentName);
        let daughterOrganizationId = await this.addOrganization(daughterName);
        return {parentId: parentOrganizationId, daughterId: daughterOrganizationId}
    }

    async addOrganizationRelationship(parentName, daughterName) {
        let relationshipNodes = await this.initializeOrganizationRelationshipNodes(parentName, daughterName);

        let existingRelationship = await this.getRelationship(relationshipNodes.parentId, relationshipNodes.daughterId);
        if(existingRelationship){
            return existingRelationship;
        }

        let connection = await this.getConnection();
        let relationship = await connection.query("INSERT INTO relation(parent_organization, daughter_organization) VALUES (?, ?)", [relationshipNodes.parentId, relationshipNodes.daughterId]);
        connection.release();
        return relationship;
    }

    async getRelationship(parentName, daughterName){
        let connection = await this.getConnection();
        let relation = await connection.query("select * from relation where parent_organization = ? and daughter_organization = ?", [parentName, daughterName]);
        connection.release();
        return relation[0];
    }

    /**
     * This method runs 3 different SQL queries, unions them and then sort them by organization name
     * Query 1: Get parent organizations
     * Query 2: get daughter organizations
     * Query 3: get sister organizations
     * @param organizationName
     * @returns {Promise<*>}
     */
    async getRelatedOrganizations(organizationName){
        let connection = await this.getConnection();
        let relatives = await connection.query(
            "(select parent.name as org_name, 'parent' as relationship_type\n" +
            "from relation r\n" +
            "    join organization parent on r.parent_organization = parent.id\n" +
            "    join organization daughter on r.daughter_organization = daughter.id\n" +
            "where daughter.name = ?)\n" +
            "UNION\n" +
            "(select daughter.name as org_name, 'daughter' as relationship_type\n" +
            "from relation r\n" +
            "    join organization parent on r.parent_organization = parent.id\n" +
            "    join organization daughter on r.daughter_organization = daughter.id\n" +
            "where parent.name = ?)\n" +
            "UNION\n" +
            "(select distinct daughter.name as org_name, 'sister' as relationship_type\n" +
            "from relation r\n" +
            "         join organization parent on r.parent_organization = parent.id\n" +
            "         join organization daughter on r.daughter_organization = daughter.id\n" +
            "where parent.id in (select parent.id\n" +
            "                    from relation r\n" +
            "                             join organization parent on r.parent_organization = parent.id\n" +
            "                             join organization daughter on r.daughter_organization = daughter.id\n" +
            "                    where daughter.name = ?)\n" +
            "and daughter.name != ?)\n" +
            "order by org_name",[organizationName,organizationName,organizationName,organizationName]);
        connection.release();
        return relatives;
    }
};

