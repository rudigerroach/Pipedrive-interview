CREATE DATABASE /*!32312 IF NOT EXISTS*/ `organizations` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `organizations`;

create table if not exists organization
(
    id   int auto_increment
        primary key,
    name varchar(50) not null,
    constraint organization_name_uindex
        unique (name)
);

create table if not exists relation
(
    id                    int auto_increment
        primary key,
    parent_organization   int null,
    daughter_organization int null,
    constraint daughter_organization___fk
        foreign key (daughter_organization) references organization (id),
    constraint parent_organization___fk
        foreign key (parent_organization) references organization (id)
);
