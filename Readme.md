Running instructions:
1. Pull the repo
2. Install docker and docker-compose
3. run 'docker-compose up' in the root of the project

Performance considerations:
Overall I think that the service will handle insertion load well, but retrieval performance will be poor under heavier load.
In retrieval I see two bottlenecks:
1. The retrieval query has to do a lot of joining to and re-joining to get the relationships right. I am sure that there should be a more efficient way to perform retrievals.
2. During pagination the entire set of related organizations is loaded into memory and then filtered into page. To scale, It would probably be good to adjust the SQL query to get a smaller subset into memory at a time.

To scale to 1M relations I would consider moving away from an SQL database and look at something like MongoDB where I could explicitly name the relationships between organizations, thereby eliminating the need for those expensive joins mentioned above.